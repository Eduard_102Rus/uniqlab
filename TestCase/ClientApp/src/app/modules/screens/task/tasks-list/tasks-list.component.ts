import { Component, OnInit, OnDestroy, } from '@angular/core';
import { TaskService } from '../task.service';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { CreateTask } from '../task.model';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.css']
})
export class TasksListComponent implements OnInit, OnDestroy {

  constructor(private tasksService: TaskService, private router: Router) {

  }

  tasksList: Observable<CreateTask[]>;
  isTasksListEmpty = true;

  ngOnInit() {

    
    this.tasksService.getTasks().subscribe(resp => {

        console.log(resp);
      this.tasksList = resp;
    });
    
  }

  ngOnDestroy() {
    
  }

  getTask(id: number) {
    this.tasksService.taskEditId(id);
  }

  delete(id: number) {
    this.tasksService.delete(id).subscribe(r => {
      this.router.navigateByUrl('/'); 
    })
  }
}
