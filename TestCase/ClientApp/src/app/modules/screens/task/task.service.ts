import { Injectable } from '@angular/core';
import { CreateTask } from './task.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { TaskEdit } from './task-edit.model';

@Injectable()
export class TaskService {

  constructor(private http: HttpClient) { }

  tasksList: Observable<any>;

  createTask(taskModel: CreateTask) {
    return this.http.post('/api/task/create', taskModel);
  }

  getTasks(): Observable<any> {
    return this.http.get<any>('/api/task/tasks');
  }


  delete(id: number) {
   return this.http.post('/api/task/delete', id);
  }
  taskEditId(id: number): Observable<any> {
    return this.http.get<any>('api/task/task/' + id);
  }

  taskEdit(taskModel: TaskEdit) {
    return this.http.post<any>('/api/task/edit', taskModel);
  }

  

}
