import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { TaskCreateComponent } from './task-create/task-create.component';
import { TaskEditComponent } from './task-edit/task-edit.component';

import { patch } from 'webdriver-js-extender';


const routes: Routes = [
  {
    path: '',
    component: TasksListComponent
  },
  {
    path: 'new',
    component: TaskCreateComponent
  },
  {
    path: 'edit',
    component: TaskEditComponent
  }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class TaskRoutingModule { }
